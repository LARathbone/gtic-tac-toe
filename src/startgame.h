#ifndef __STARTGAME_H
#define __STARTGAME_H

#include <stdio.h>
#include <gtk/gtk.h>
#include "board.h"

/* function decls */
GtkWidget* create_start_game_dialog(void);
int start_game_dialog_get_choice(void);

#endif
