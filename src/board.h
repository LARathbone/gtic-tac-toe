#ifndef __BOARD_H
#define __BOARD_H

#include <stdio.h>
#include <stdlib.h>
#include <glib.h>

#define NO_VAL	0
#define X_VAL	1
#define O_VAL	2

int* init_board(int len);
int coord_to_pos(int x, int y, int len);
void destroy_board(int* board);
int check_win(int* board, int x, int y, int len);
int check_board_full(int* board, int len);
int comp_take_turn(int* board, int len, int comp);
int opposite(int player);
int posn_to_y(int posn, int len);
int posn_to_x(int posn, int len);

#endif		/* __BOARD_H */
