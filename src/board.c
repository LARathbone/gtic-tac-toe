#include "board.h"

/* NOTE: just a test function. */

/* Here's a grid for ease of reference.
 *
 *     0 1 2
 *   0 x o x
 *   1 o o x
 *   2 o x o
*/

/*
int main(void) {
	int* board = init_board(3);
	int test;


	board[coord_to_pos(0, 0, 3)] = X_VAL;
	board[coord_to_pos(0, 1, 3)] = X_VAL;
	board[coord_to_pos(0, 2, 3)] = X_VAL;

	board[coord_to_pos(1, 0, 3)] = O_VAL;
	board[coord_to_pos(1, 1, 3)] = O_VAL;
	board[coord_to_pos(1, 2, 3)] = X_VAL;

	board[coord_to_pos(2, 0, 3)] = O_VAL;
	board[coord_to_pos(2, 2, 3)] = X_VAL;

	if ( check_win(board, 0, 0, 3) == 0 && check_board_full(board, 3) ) {
		printf("Cat's game.\n");
	}

	if ( check_win(board, 0, 0, 3) > 0 && check_board_full(board, 3) ) {
		printf("Board full but someone won.\n");
	}

	test = comp_take_turn(board, 3);
	printf("Turn for computer will be: %d\n", test);

	printf("This turn is: (%d , %d) as converted.\n", posn_to_x(test, 3), posn_to_y(test, 3));

	return 0;
}
*/

int* init_board(int len) {
	int board_size = len * len;
	int* board;
	int i;

	/* allocate enough memory for the ints required by the board */
	board = (int*)( malloc(board_size*sizeof(int)) );

	/* populate board with NO_VALs */
	printf("DEBUG: Initializing board with NO_VALS...\n");
	for (i = 0; i < board_size; ++i) {
		board[i] = NO_VAL;
		printf("DEBUG: board[%d]: %d\n", i, board[i]);
	}

	return board;
}

int coord_to_pos(int x, int y, int len) {
	int pos = (x * len) + y;
	return pos;
}

void destroy_board(int* board) {
	free(board);
	/* free does not return a value so there is no testing to see if this worked */
}

/* check for a win after (x,y) spot on board has been altered */
int check_win(int* board, int x, int y, int len) {
	int i;
	int ct_x_X, ct_x_O, ct_y_X, ct_y_O;
	int ct_diag1_X, ct_diag1_O, ct_diag2_X, ct_diag2_O;

	printf("DEBUG: checking for win.\n");

	ct_x_X = 0;
	ct_x_O = 0;
	ct_y_X = 0;
	ct_y_O = 0;

	ct_diag1_X = 0;
	ct_diag1_O = 0;
	ct_diag2_X = 0;
	ct_diag2_O = 0;


	for (i = 0; i < len; ++i) {
		/* check row as specified by x */	
		if (board[coord_to_pos(x, i, len)] == X_VAL) ++ct_x_X;
		if (board[coord_to_pos(x, i, len)] == O_VAL) ++ct_x_O;

		/* check col as specified by y */	
		if (board[coord_to_pos(i, y, len)] == X_VAL) ++ct_y_X;
		if (board[coord_to_pos(i, y, len)] == O_VAL) ++ct_y_O;

		/* now we check for diagonals. This does not utilize the x and y vals
		 * provided at all, but it seems cheap enough to do every time.
		 * Possible FIXME for optimization. */

		/* check diag1 (top-left to bot-right)
		 * coords will always be equal as we go up the chain ie, (0,0), (1,1)...(n,n) */

		if (board[coord_to_pos(i, i, len)] == X_VAL) ++ct_diag1_X;
		if (board[coord_to_pos(i, i, len)] == O_VAL) ++ct_diag1_O;

		/* check diag2 (bot-left to top-right):  ((len-1),0), ((len-1)-1,0+1), ((len-1)-2,0+2), ...
		 * 						( ((len-1)-(len-1)), (len-1) ) */

		if ( board[coord_to_pos( ((len-1)-i), i, len )] == X_VAL ) ++ct_diag2_X;
		if ( board[coord_to_pos( ((len-1)-i), i, len )] == O_VAL ) ++ct_diag2_O;

/*		printf("DEBUG: i: %d - ct_x_X: %d - ct_x_O: %d - ct_y_X: %d - ct_y_O: %d\n",
				i, ct_x_X, ct_x_O, ct_y_X, ct_y_O); */

		/* printf("DEBUG: i: %d - ct_diag1_X: %d - ct_diag1_O: %d - ct_diag2_X: %d - ct_diag2_O: %d\n", 
				i, ct_diag1_X, ct_diag1_O, ct_diag2_X, ct_diag2_O); */

		/* ok we've reached the end of the row/col... */
		if ( i == (len-1) ) {
			if (ct_x_X == len) {
				printf("DEBUG: X wins the row.\n");
				return X_VAL;
			} else if (ct_x_O == len) {
				printf("DEBUG: O wins the row.\n");
				return O_VAL;
			} else if (ct_y_X == len) {
				printf("DEBUG: X wins the column.\n");
				return X_VAL;
			} else if (ct_y_O == len) {
				printf("DEBUG: O wins the column.\n");
				return O_VAL;
			} else if (ct_diag1_X == len) {
				printf("DEBUG: X wins the diagonal (top-left to bot-right.\n");
				return X_VAL;
			} else if (ct_diag1_O == len) {
				printf("DEBUG: O wins the diagonal (top-left to bot-right.\n");
				return O_VAL;
			} else if (ct_diag2_X == len) {
				printf("DEBUG: X wins the diagonal (bot-left to top-right.\n");
				return X_VAL;
			} else if (ct_diag2_O == len) {
				printf("DEBUG: O wins the diagonal (bot-left to top-right.\n");
				return O_VAL;
			}
		} /* i == len-1 */
	}

	printf("DEBUG: no winner found.\n");
	return FALSE;
}

/* run through the whole board and see if it's been filled up.
 * Used to check for ties. */
int check_board_full(int* board, int len) {
	int board_size = len * len;
	int i;

	for (i = 0; i < board_size; ++i) {
		if (board[i] == 0) {
			printf("DEBUG: Found blank square. Board not full.\n");
			return FALSE;
		} else if (board[i] > 0) {
			continue;
		} else {
			fprintf(stderr, "ERROR. BOARD VALUE MISDEFINED.\n");
			return -1;
		}
	}
	printf("DEBUG: Board full detected.\n");
	return TRUE;
}

/* generates an integer to be plugged back into board* for where the
 * computer played. We need to know the length of the board and which
 * letter the computer is playing as.
 *
 * returns -1 if no valid moves are available.
 *
 * Based on code written by Dotz0cat https://github.com/Dotz0cat/C
 *
*/

int comp_take_turn(int* board, int len, int comp) {
	int board_size = len * len;
	gint32 r;			/* don't panic, this is just a signed int. */
	int player = opposite(comp);

	/* a bit of rudimentary AI if we're on a standard 3x3 board. */

	if (len == 3) {
		if ( check_board_full(board, len) == FALSE ) {
			if (board[0] == player &&
					board[1] == player &&
					board[2] == NO_VAL)	return 2;

			if (board[3] == player &&
					board[4] == player &&
					board[5] == NO_VAL)	return 5;

			if (board[6] == player &&
					board[7] == player &&
					board[8] == NO_VAL)	return 8;
			
			if (board[0] == player &&
					board[3] == player &&
					board[6] == NO_VAL)	return 6;

			if (board[1] == player &&
					board[4] == player &&
					board[7] == NO_VAL)	return 7;

			if (board[2] == player &&
					board[5] == player &&
					board[8] == NO_VAL)	return 8;

			if (board[1] == player &&
					board[2] == player &&
					board[0] == NO_VAL)	return 0;

			if (board[4] == player &&
					board[5] == player &&
					board[3] == NO_VAL)	return 3;

			if (board[7] == player &&
					board[8] == player &&
					board[6] == NO_VAL)	return 6;
			
			if (board[3] == player &&
					board[6] == player &&
					board[0] == NO_VAL)	return 0;

			if (board[4] == player &&
					board[7] == player &&
					board[1] == NO_VAL)	return 1;

			if (board[5] == player &&
					board[8] == player &&
					board[2] == NO_VAL)	return 2;

			if (board[0] == player &&
					board[4] == player &&
					board[8] == NO_VAL)	return 8;

			if (board[0] == player &&
					board[8] == player &&
					board[4] == NO_VAL)	return 4;

			if (board[4] == player &&
					board[8] == player &&
					board[0] == NO_VAL)	return 0;

			if (board[6] == player &&
					board[4] == player &&
					board[2] == NO_VAL)	return 2;

			if (board[6] == player &&
					board[2] == player &&
					board[4] == NO_VAL)	return 4;

			if (board[4] == player &&
					board[2] == player &&
					board[6] == NO_VAL)	return 6;

			if (board[0] == player &&
					board[2] == player &&
					board[1] == NO_VAL)	return 1;

			if (board[3] == player &&
					board[5] == player &&
					board[4] == NO_VAL)	return 4;

			if (board[6] == player &&
					board[8] == player &&
					board[7] == NO_VAL)	return 7;

			if (board[0] == player &&
					board[6] == player &&
					board[3] == NO_VAL)	return 3;

			if (board[1] == player &&
					board[7] == player &&
					board[4] == NO_VAL)	return 4;

			if (board[2] == player &&
					board[8] == player &&
					board[5] == NO_VAL)	return 5;

		}	// check_board_full FALSE
	}	// len 3

	/* if none of the AI applies, just pick an empty square randomly. */

	while ( check_board_full(board, len) == FALSE ) {

		/* generate a random number between zero and one less than board_size */
		r = g_random_int_range(0, board_size);

		printf("DEBUG: Trying board[%d] as a valid move for computer...\n", r);

		if (board[r] == NO_VAL) {
			printf("DEBUG: Valid move for comp found: board[%d].\n", r);
			return r;
		} 
	}
	printf("DEBUG: Couldn't find a valid move for the computer.\n");
	return -1;
}

int opposite(int player) {
	int opp;

	if (player == X_VAL) {
		opp = O_VAL;
	} else if (player == O_VAL) {
		opp = X_VAL;
	} else {
		fprintf(stderr, "ERROR: Invalid usage of opposite function. Program may fail from here.\n");
		return -1;
	}

	printf("DEBUG: opposite of %d is %d. Returning value by request.\n", player, opp);

	return opp;
}

int posn_to_y(int posn, int len) {
	int y;
	y = posn % len;
	return y;
}

int posn_to_x(int posn, int len) {
	int x;
	x = posn / len;
	return x;
}
