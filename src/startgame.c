#include "startgame.h"

/* set default of 'choice' */
/* FIXME seems like a poor way to handle this. */
static int choice = X_VAL;

/* global vars */
static GtkWidget* start_game_dialog;
static GtkWidget* x_radiobutton;
static GtkWidget* o_radiobutton;
static GtkWidget* random_radiobutton;

/* BEGIN FUNCTIONS _NOT_ SET UP BY GLADE */

/* Initialize the start game dialog.
 * NOTE - run this function before any others! */
GtkWidget* create_start_game_dialog(void) {
	/* GTK vars declared */	
	GtkBuilder*	builder;

	/* initialize builder - this CANNOT be done as a global var. */
	gchar* glade_gui = g_build_filename(DATA_DIRECTORY, "gtictactoe.glade", NULL);
	builder = gtk_builder_new_from_file(glade_gui);
	g_free(glade_gui);

	/* intialize our base window - uses global var defd in header */	
	start_game_dialog = GTK_WIDGET(gtk_builder_get_object(builder, "start_game_dialog"));

	/* initialize radio buttons to be used by other functions */
	x_radiobutton = GTK_WIDGET(gtk_builder_get_object(builder, "x_radiobutton"));
	o_radiobutton = GTK_WIDGET(gtk_builder_get_object(builder, "o_radiobutton"));
	random_radiobutton = GTK_WIDGET(gtk_builder_get_object(builder, "random_radiobutton"));

	/* connect signals */	
	gtk_builder_connect_signals(builder, NULL);

	/* CUSTOM MEAT AND POTATOTES */

	/* --- */

	/* BOILERPLATE FOR END OF MAIN FCN */

	/* boilerplate - once done with 'builder' - engage the GObject destructor */
	g_object_unref(builder);

	return start_game_dialog;
}

int start_game_dialog_get_choice(void) {
	/* NO_VAL, X_VAL, O_VAL */
	
	if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(x_radiobutton)) == TRUE ) {
		printf("DEBUG_STARTGAME: x radiobutton toggled detected.\n");
		choice = X_VAL; 
	} else if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(o_radiobutton)) == TRUE ) {
		printf("DEBUG_STARTGAME: o radiobutton toggled detected.\n");
		choice = O_VAL; 
	} else if ( gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(random_radiobutton)) == TRUE ) {
		printf("DEBUG_STARTGAME: random radiobutton toggled detected.\n");
		choice = g_random_int_range(1, 2);	/* FIXME - this assumes X_ and O_VAL will
							   always be fixed as 1 and 2 */
	} else {	/* default to X if some weirdo error - POLA */
		printf("DEBUG_STARTGAME: for some reason no radiobuttons are toggled. Defaulting to X.\n");
		choice = X_VAL; 
	}
	printf("DEBUG_STARTGAME: value of choice: %d\n", choice);
	return choice;
}


/* BEGIN FUNCTIONS SET UP BY GLADE 
 * FIXME - shouldn't these need to be declared b4 main() ? */

/* called when window is closed */

/* void on_start_game_dialog_destroy(void) {
	printf("DEBUG: User closed startgame window instead of picking X's or O's. Going with default.\n");
	choice = X_VAL;
} */

/* void on_start_game_button_clicked(void) {
	start_game_dialog_get_choice();
} */
