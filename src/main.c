#include <stdio.h>
#include <gtk/gtk.h>
#include "board.h"
#include "startgame.h"

/* --------------------- */
/* function declarations */
/* --------------------- */

/* boilerplate gtk */
static void activate(GtkApplication* app, gpointer user_data);

/* custom */
static void update_board_gui(void);
static gchar* board_val_to_str(int val);
static void activate_buttons_for_players_turn(void);
static void gui_comp_take_turn(void);
static void new_game(void);
static void eval_player_move(int x, int y);
static void eval_comp_move(int x, int y);
static void game_over(int winner);
static void ask_x_or_o(void);
static void gui_players_turn(void);
static gboolean timer_handler(void);
static void grey_out_board(void);
static void game_over_dialog_closed(GtkWidget* dialog);

/* ---------------- */
/* global variables */
/* ---------------- */

static int* board;

/* define length of board as 3 
 * FIXME - make this changeable */
static int len = 3;

/* whether the player is x or o */
static int x_or_o = X_VAL;
/* whether the comp is x or o */
static int comp = O_VAL;

/* scores of player and comp. */
static unsigned int player_score = 0;
static unsigned int comp_score = 0;

/* timer required for fake computer thinking */
gboolean comp_thinking = FALSE;

/* global widgets we need access to */
/* FIXME - this is dumb. */
static GtkWidget*	window;
//static GtkApplication*	app;

static GtkWidget* button_1_1;
static GtkWidget* button_1_2;
static GtkWidget* button_1_3;
static GtkWidget* button_2_1;
static GtkWidget* button_2_2;
static GtkWidget* button_2_3;
static GtkWidget* button_3_1;
static GtkWidget* button_3_2;
static GtkWidget* button_3_3;

static GtkWidget* player_xoro_label;
static GtkWidget* comp_xoro_label;
static GtkWidget* whose_turn_label;
static GtkWidget* player_score_label;
static GtkWidget* comp_score_label;
/* --- */

int main(int argc, char *argv[]) {
	/* GTK vars declared */	
	GtkApplication*	app;
	int status;

	/* boilerplate GTK cruft */
	app = gtk_application_new("com.gitlab.larathbone.gtic-tac-toe", G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);

	/* starts main loop */
	status = g_application_run( G_APPLICATION(app), argc, argv );

	printf("DEBUG_GUI: g_application_run call DONE\n");

	/* destroy app gobject one main loop done */
	g_object_unref(app);

	/* ok, main loop is done and we have a return 'status' defined. */
	printf("DEBUG_GUI: main: destroying board... \n");
	destroy_board(board);

	printf("DEBUG_GUI: returning: %d\n", status);
	return status;
}	/* main */

/* GTK activation function */
void activate (GtkApplication* app, gpointer user_data) {
	/* BOILERPLATE */
	GtkBuilder* builder;
	gchar* glade_gui;

	/* initialize builder - this CANNOT be done as a global var. */
	glade_gui = g_build_filename(DATA_DIRECTORY, "gtictactoe.glade", NULL);
	printf("DEBUG_GUI: glade_gui: %s\n", glade_gui);
	builder = gtk_builder_new_from_file(glade_gui);
	g_free(glade_gui);

	/* intialize our base window - uses global var defd above */	
	window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));

	/* connect signals */	
	gtk_builder_connect_signals(builder, NULL);

	gtk_window_set_application(GTK_WINDOW(window), app);

	/* BEGIN CUSTOM MEAT AND POTATOTES */

	/* initialize a timer (used for fake computer "thinking" ; id unused
	 * presently - last argument can take anything, eg, widget to do things with */
	guint timer_id = g_timeout_add_seconds(3, G_SOURCE_FUNC(timer_handler), NULL);

	/* setup tic-tac-toe buttons we need to manipulate */

#define BUILD_TTT_BUTTON(X, Y) \
	button_ ## X ## _ ## Y = GTK_WIDGET(gtk_builder_get_object(builder, "button_" #X "_" #Y));

	BUILD_TTT_BUTTON(1, 1) BUILD_TTT_BUTTON(1, 2) BUILD_TTT_BUTTON(1, 3)
	BUILD_TTT_BUTTON(2, 1) BUILD_TTT_BUTTON(2, 2) BUILD_TTT_BUTTON(2, 3)
	BUILD_TTT_BUTTON(3, 1) BUILD_TTT_BUTTON(3, 2) BUILD_TTT_BUTTON(3, 3)

	/* setup labels we need to manipulate */

	player_xoro_label = GTK_WIDGET(gtk_builder_get_object(builder, "player_xoro_label"));
	comp_xoro_label = GTK_WIDGET(gtk_builder_get_object(builder, "comp_xoro_label"));
	whose_turn_label = GTK_WIDGET(gtk_builder_get_object(builder, "whose_turn_label"));
	player_score_label = GTK_WIDGET(gtk_builder_get_object(builder, "player_score_label"));
	comp_score_label = GTK_WIDGET(gtk_builder_get_object(builder, "comp_score_label"));

	/* set score labels to 0 - probably not necessary but I like to be explicit. */
	gtk_label_set_text(GTK_LABEL(player_score_label),
			g_strdup_printf("0"));

	gtk_label_set_text(GTK_LABEL(comp_score_label),
			g_strdup_printf("0"));

	/* END CUSTOM MEAT AND POTATOES */

	/* once done with 'builder' - engage the GObject destructor */
	g_object_unref(builder);

	/* show main window. */
	gtk_widget_show_all(window);

	/* before starting first game, find out whether player wants to be x's or o's */
	ask_x_or_o();

	/* initialize game */
	new_game();
}	/* activate */

/* BEGIN FUNCTIONS SET UP BY GLADE 
 * FIXME - shouldn't these need to be declared b4 main() ? */

/* called when window is closed */

void on_window_main_destroy(void)
{
	printf("DEBUG_GUI: running on_window_main_destroy\n");
//	g_application_quit(G_APPLICATION(app));
	printf("DEBUG_GUI: [on_window_main_destroy DONE]\n");
}

/* called when buttons clicked */
#define ON_BUTTON_CLICKED(X, Y) \
	printf("DEBUG_GUI: running on_button_" #X "_" #Y "_clicked... "); \
	board[coord_to_pos(X - 1, Y - 1, len)] = x_or_o; \
	update_board_gui(); \
	eval_player_move(X - 1, Y - 1); \
	printf("DEBUG_GUI: [button_" #X "_" #Y "_clicked DONE]\n");

void on_button_1_1_clicked(void) {
	ON_BUTTON_CLICKED(1, 1)
}

void on_button_1_2_clicked(void) {
	ON_BUTTON_CLICKED(1, 2)
}

void on_button_1_3_clicked(void) {
	ON_BUTTON_CLICKED(1, 3)	
}

void on_button_2_1_clicked(void) {
	ON_BUTTON_CLICKED(2, 1)
}

void on_button_2_2_clicked(void) {
	ON_BUTTON_CLICKED(2, 2)
}

void on_button_2_3_clicked(void) {
	ON_BUTTON_CLICKED(2, 3)
}

void on_button_3_1_clicked(void) {
	ON_BUTTON_CLICKED(3, 1)
}

void on_button_3_2_clicked(void) {
	ON_BUTTON_CLICKED(3, 2)
}

void on_button_3_3_clicked(void) {
	ON_BUTTON_CLICKED(3, 3)
}


/* BEGIN FUNCTIONS _NOT_ SET UP BY GLADE */

void update_board_gui(void) {
	gchar* pstr;	/* player's who's-who string */
	gchar* cstr;	/* comp's who's-who string */
	gchar* tmpstr;	/* temp string to update TTT button labels */

	printf("DEBUG_GUI: running update_board_gui... ");

	/* set button labels based on the values of the board array. */

#define UPDATE_BTN_LABEL(X, Y, Z) \
	tmpstr = (board_val_to_str(board[Z])); \
	gtk_button_set_label(GTK_BUTTON(button_ ## X ## _ ## Y), tmpstr); \
	g_free(tmpstr);

	UPDATE_BTN_LABEL(1, 1, 0) UPDATE_BTN_LABEL(1, 2, 1) UPDATE_BTN_LABEL(1, 3, 2)
	UPDATE_BTN_LABEL(2, 1, 3) UPDATE_BTN_LABEL(2, 2, 4) UPDATE_BTN_LABEL(2, 3, 5)
	UPDATE_BTN_LABEL(3, 1, 6) UPDATE_BTN_LABEL(3, 2, 7) UPDATE_BTN_LABEL(3, 3, 8)


	/* update 'who's who' label */
	if (x_or_o == X_VAL) {
		pstr = g_strdup_printf("X");
		cstr = g_strdup_printf("O");
	} else {
		pstr = g_strdup_printf("O");
		cstr = g_strdup_printf("X");
	}
	gtk_label_set_text(GTK_LABEL(player_xoro_label), pstr);
	gtk_label_set_text(GTK_LABEL(comp_xoro_label), cstr);
	g_free(pstr);
	g_free(cstr);
	
	printf("DEBUG_GUI: [update_board_gui DONE]\n");
}	/* update_board_gui */

gchar* board_val_to_str(int val) {
	gchar* string;	
	
	if ( val == NO_VAL ) {
		string = g_strdup_printf(" ");
	} else if ( val == X_VAL ) {
		string = g_strdup_printf("X");
	} else if ( val == O_VAL ) {
		string = g_strdup_printf("O");
	} else {
		string = g_strdup_printf("??");
	}

	return string;
}

void activate_buttons_for_players_turn(void) {
	/* activate the buttonz */
	/* FIXME - this is based on a 3x3 fixed board atm. */
	 
	if (board[0] == NO_VAL) {
		gtk_widget_set_sensitive(button_1_1, TRUE);
	} else {
		gtk_widget_set_sensitive(button_1_1, FALSE);
	}

	if (board[1] == NO_VAL) {
		gtk_widget_set_sensitive(button_1_2, TRUE);
	} else {
		gtk_widget_set_sensitive(button_1_2, FALSE);
	}

	if (board[2] == NO_VAL) {
		gtk_widget_set_sensitive(button_1_3, TRUE);
	} else {
		gtk_widget_set_sensitive(button_1_3, FALSE);
	}

	if (board[3] == NO_VAL) {
		gtk_widget_set_sensitive(button_2_1, TRUE);
	} else {
		gtk_widget_set_sensitive(button_2_1, FALSE);
	}

	if (board[4] == NO_VAL) {
		gtk_widget_set_sensitive(button_2_2, TRUE);
	} else {
		gtk_widget_set_sensitive(button_2_2, FALSE);
	}

	if (board[5] == NO_VAL) {
		gtk_widget_set_sensitive(button_2_3, TRUE);
	} else {
		gtk_widget_set_sensitive(button_2_3, FALSE);
	}

	if (board[6] == NO_VAL) {
		gtk_widget_set_sensitive(button_3_1, TRUE);
	} else {
		gtk_widget_set_sensitive(button_3_1, FALSE);
	}

	if (board[7] == NO_VAL) {
		gtk_widget_set_sensitive(button_3_2, TRUE);
	} else {
		gtk_widget_set_sensitive(button_3_2, FALSE);
	}

	if (board[8] == NO_VAL) {
		gtk_widget_set_sensitive(button_3_3, TRUE);
	} else {
		gtk_widget_set_sensitive(button_3_3, FALSE);
	}
}

void gui_comp_take_turn(void) {
	printf("DEBUG_GUI: running gui_comp_take_turn... \n");

	gtk_label_set_text(GTK_LABEL(whose_turn_label), "Computer's Turn...");

	printf("DEBUG_GUI: gui_comp_take_turn setting comp_thinking to TRUE\n");
	comp_thinking = TRUE;
	grey_out_board();
	printf("DEBUG_GUI: [gui_comp_take_turn DONE]\n");
}


void new_game(void) {
	printf("DEBUG_GUI: Entering new_game routine... \n");

	/* initialize board - declared as a global above. */
	printf("DEBUG_GUI: Initializing board.\n");
	board = init_board(len);

	/* make the computer the opposite of what the player is (X/O). */	
	comp = opposite(x_or_o);
	printf("DEBUG_GUI: player is: %d - computer is: %d\n", x_or_o, comp);

	/* update board gui - if we don't do this, board will still be
	 * populated for a few seconds if this is a rematch and computer goes first */
	update_board_gui();

	/* if computer is 'X', they take the first turn. */
	if (comp == X_VAL)	gui_comp_take_turn();
	/* otherwise, player is X and takes first turn. */
	else			gui_players_turn();

	printf("DEBUG_GUI: [new_game DONE]\n");
}	/* new_game */

void eval_player_move(int x, int y) {
	if ( check_win(board, x, y, len) != x_or_o ) {
		if ( check_board_full(board, len) == TRUE ) {
			/* cat's game */
			game_over(NO_VAL);
		} else {
			/* no winner and board not full - computer can take a turn */
			gui_comp_take_turn();
		}
	} else {
		game_over(x_or_o);
	}
}

void eval_comp_move(int x, int y) {
	if ( check_win(board, x, y, len) != comp ) {
		if ( check_board_full(board, len) == TRUE ) {
			/* cat's game */
			game_over(NO_VAL);
		} else {
			/* no winner and board not full - player can take a turn */
			gui_players_turn();
		}
	} else {
		game_over(comp);
	}
}

/* this function starts the endgame process. Note that game_over_dialog_closed
 * is what truly ends the game over process, but if we don't split it up in 2
 * fcns and have the 2nd part a callback from the dialog box, running the dialog
 * from the glib timer as we essentially do will block the timer's subroutine from
 * finishing. */
void game_over(int winner) {
	GtkWidget* dialog;
	gchar* string;

	printf("DEBUG_GUI: Entering game_over routine... \n");

	/* comp stops thinking when the game is over. */
	printf("DEBUG_GUI: game_over: setting comp_thinking to FALSE\n");
	comp_thinking = FALSE;

	update_board_gui();

	grey_out_board();

	/* FIXME - allow for the player to have the option of a rematch or quit */
	if (winner == x_or_o) {
		string = g_strdup_printf("A WINNER IS YOU!");
		++player_score;
	} else if (winner == comp) {
		string = g_strdup_printf("COMPUTER WINS - YOU SUCK!");
		++comp_score;
	} else if (winner == NO_VAL) {
		string = g_strdup_printf("CAT'S GAME. (^.^)");
	} else {
		string = g_strdup_printf("Undefined error.");
	}

	printf("DEBUG_GUI: game_over: GUI knows who won: %d - msg will be: %s\n", winner, string);

	/* update score labels */
	gtk_label_set_text(GTK_LABEL(player_score_label),
			g_strdup_printf("%d", player_score));
	printf("DEBUG_GUI: game_over: player score updated.\n");

	gtk_label_set_text(GTK_LABEL(comp_score_label),
			g_strdup_printf("%d", comp_score));
	printf("DEBUG_GUI: game_over: comp score updated.\n");

	/* game over dialog */
	printf("DEBUG_GUI: game_over: creating game over dialog... ");
	dialog = gtk_message_dialog_new_with_markup(GTK_WINDOW(window),
			GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
			GTK_MESSAGE_INFO,
			GTK_BUTTONS_CLOSE,
			"<b>GAME OVER</b>\n\n%s\n\nGame will restart as a rematch.", string);

	g_signal_connect(dialog, "response",
			G_CALLBACK(game_over_dialog_closed),
			NULL);

	gtk_widget_show(dialog);

	printf("DEBUG_GUI: [game_over dialog DONE]\n");


	printf("DEBUG_GUI: [game_over DONE]\n");
}	/* game over */

void ask_x_or_o(void) {
	/* note: utilizes global ' x_or_o ' */
	GtkWidget* start_game_dialog;
	gint dialog_return;

	start_game_dialog = create_start_game_dialog();			/* def'd in startgame.h */

	/* make start_game_dialog child of window */
	gtk_window_set_transient_for( GTK_WINDOW(start_game_dialog), GTK_WINDOW(window) );

	/* this fcn returns a number of gtk-specific possibilities. we take that and
	 * finesse it into setting x_or_o as X_VAL, O_VAL or NO_VAL. */
	dialog_return = gtk_dialog_run( GTK_DIALOG(start_game_dialog) );
	switch (dialog_return) {
		case GTK_RESPONSE_ACCEPT:
			printf("DEBUG_GUI: User clicked 'Start Game' button.\n");
			x_or_o = start_game_dialog_get_choice();	/* def'd in startgame.h */
			break;
		default:
			printf("DEBUG_GUI: User closed the window without making choice. Assuming X.\n");
			x_or_o = X_VAL;	
			break;
	}
	printf("DEBUG_GUI: x_or_o has been set to: %d\n", x_or_o);
	gtk_widget_destroy(start_game_dialog);
}

void gui_players_turn(void) {
	printf("DEBUG_GUI: gui_players_turn starting... \n");
	/* once comp stops thinking when the player's turn begins. */
	printf("DEBUG_GUI: gui_players_turn: setting comp_thinking to FALSE\n");
	comp_thinking = FALSE;

	gtk_label_set_text(GTK_LABEL(whose_turn_label), "Your Turn");

	update_board_gui();
	activate_buttons_for_players_turn();

	printf("DEBUG_GUI: [gui_players_turn DONE]\n");
}

gboolean timer_handler(void) {
	int comps_move;
	int comp_x;
	int comp_y;

	printf("DEBUG_GUI: running timer_handler. comp_thinking: %d\n", comp_thinking);

	if (comp_thinking == TRUE) {
		comps_move = comp_take_turn(board, len, comp);
		printf("DEBUG_GUI: result of comps_move: %d\n", comps_move);
		
		if (comps_move != -1) {
			printf("DEBUG_GUI: detected valid move for comp...\n");
			board[comps_move] = comp;
			
			comp_x = posn_to_x(comps_move, len);
			comp_y = posn_to_y(comps_move, len);
	
			eval_comp_move(comp_x, comp_y);
		} else {
			printf("DEBUG_GUI: Comp has no valid moves. Ending game...\n");
			game_over(NO_VAL);
		}
	}

	printf("DEBUG_GUI: [timer_handler DONE]\n");
	return TRUE;	/* FALSE kills the timer */
}

void grey_out_board(void) {
	printf("DEBUG_GUI: running grey_out_board...  ");
	gtk_widget_set_sensitive(button_1_1, FALSE);
	gtk_widget_set_sensitive(button_1_2, FALSE);
	gtk_widget_set_sensitive(button_1_3, FALSE);

	gtk_widget_set_sensitive(button_2_1, FALSE);
	gtk_widget_set_sensitive(button_2_2, FALSE);
	gtk_widget_set_sensitive(button_2_3, FALSE);

	gtk_widget_set_sensitive(button_3_1, FALSE);
	gtk_widget_set_sensitive(button_3_2, FALSE);
	gtk_widget_set_sensitive(button_3_3, FALSE);
	printf("DEBUG_GUI: [grey_out_board DONE]\n");
}

/* this is basically "game_over part 2", but its running is dependent upon
 * the game over dialog box closing. */
void game_over_dialog_closed(GtkWidget* dialog) {
	printf("DEBUG_GUI: running game_over_dialog_closed\n");
	
	gtk_widget_destroy(dialog);

	/* destroy board */
	printf("DEBUG_GUI: game_over_dialog_closed: Destroying board.\n");
	destroy_board(board);

	/* swap players and start a new game. */
	printf("DEBUG_GUI: game_over_dialog_closed: Internally swapping players.\n");
	x_or_o = opposite(x_or_o);

	printf("DEBUG_GUI: game_over_dialog_closed: launching new_game subroutine.\n");
	new_game();

	printf("DEBUG_GUI: [game_over_dialog_closed DONE]\n");
}	/* game_over_dialog_closed */

