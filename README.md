# gtic-tac-toe

## IMPORTANT NOTICE

This program was written while I was in the process of learning both GTK and C. The code quality is likely quite poor and unidiomatic. It is being kept here for historical purposes (and it is a fun little game if you want to waste a bit of time), but should not be relied upon for legitimate educational purposes at this point.

## Copyright and Authorship

Author: Logan Rathbone <poprocks@gmail.com>
Copyright (C) 2020

This software is licensed under the GNU GPL v. 2.  See 'COPYING' for full
licence. As a result, this software comes with ABSOLUTELY NO WARRANTY.

Further, for greater certainty, this means that you may NOT re-license this
software under a later version of the GNU GPL at your option.

## Description

GTK3 tic-tac-toe program written in C. Simple project to try to learn basic C
and GTK concepts.

## Dependencies

glib2 >= 2.58
gtk3 >= 3.18

## Build Instructions

	./autogen.sh
		( ^-- if downloaded from git. )
	./configure
	make
	make install
		( ^-- as root. )

See 'INSTALL' for more detailed (but generic) instructions.
